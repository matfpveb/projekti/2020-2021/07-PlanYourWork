import { ProfileService } from './../services/profile.service';
import { MessageService } from './../services/message.service';
import { switchMap } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy, Inject} from '@angular/core';
import { CardService } from '../services/card.service';
import { ChecklistService } from '../services/checklist.service';
import { Card } from '../models/card.model';
import { environment } from '../../environments/environment';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ThrowStmt } from '@angular/compiler';
import { ListService } from '../services/list.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit, OnDestroy {

  listName: string = 'To Do';
  showMembers: boolean = false;
  showLabels: boolean = false;
  showChecklist: boolean = false;
  showDate: boolean = false;
  showAttachment: boolean = false;
  showCover: boolean = false;
  public card: Card;
  private activeSubscriptions: Subscription[];
  messages = [];
  boardMembers : string[]= []
  cardId: string;
  listId: string;
  projectId: string;

  constructor(public cardService: CardService,
              private dialogRef: MatDialogRef<CardComponent>,
              private messageService: MessageService,
              private profileService: ProfileService,
              private checklistService: ChecklistService,
              private listService: ListService,
              @Inject(MAT_DIALOG_DATA) public data: { cardId: string,
                                                      listId: string,
                                                      projectId: string,
                                                      listName: string
                                                    }) {
    this.activeSubscriptions = [];
    this.cardId = data.cardId;
    this.listId = data.listId;
    this.projectId = data.projectId;
    this.listName = data.listName;
    this.findCardById();
    const sub = this.messageService.getMessage().subscribe((msg) => {
      if(msg){
        this.messages.push(msg);
        this.activeSubscriptions.push(sub);
        this.findCardById();
      }
      else{
        this.messages = [];
      }
    })
    this.activeSubscriptions.push(sub);

  }

  ngOnInit(): void {
    this.findCardById();
    const sub = this.profileService.getTeamMembers(this.projectId)
                    .subscribe((team: any) => this.boardMembers = team.map(member => member.name));
    this.activeSubscriptions.push(sub);
  }

  private findCardById() {
    const getCardSub =
          this.cardService.getCardById(this.cardId)
      .subscribe((card) => (this.card = card));
    this.activeSubscriptions.push(getCardSub);
  }

  ngOnDestroy() {
    this.activeSubscriptions.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }

  onChangeTitle(newTitle: string): void{
    if (this.card.title === newTitle){
      return;
    }
    this.card.title = newTitle;

    const updateTitleSub = this.cardService.changeCardTitle(this.card._id, this.card.title).subscribe(() => {
      this.cardService.changeCardSubject.next({"cardId": this.cardId, "title": this.card.title});
    });
    this.activeSubscriptions.push(updateTitleSub);
  }

  onChangeDescription(newDesc: string): void{
    if (this.card.description === newDesc){
      return;
    }
    this.card.description = newDesc;
    const updateDescSub = this.cardService.changeCardDescription(this.card._id, this.card.description).subscribe();
    this.activeSubscriptions.push(updateDescSub);
  }

  toggleMembersCard(): void{
    this.showMembers = !this.showMembers;
    this.showLabels = false;
    this.showChecklist = false;
    this.showDate = false;
    this.showAttachment = false;
    this.showCover = false;
  }

  toggleLabelsCard(): void{
    this.showLabels = !this.showLabels;
    this.showMembers = false;
    this.showChecklist = false;
    this.showDate = false;
    this.showAttachment = false;
    this.showCover = false;
  }

  toggleChecklistCard(): void{
    this.showChecklist = !this.showChecklist;
    this.showMembers = false;
    this.showLabels = false;
    this.showDate = false;
    this.showAttachment = false;
    this.showCover = false;
  }

  toggleDateCard(): void{
    this.showDate = !this.showDate;
    this.showMembers = false;
    this.showLabels = false;
    this.showChecklist = false;
    this.showAttachment = false;
    this.showCover = false;
  }

  toggleAttachmentCard(): void{
    this.showAttachment = !this.showAttachment;
    this.showMembers = false;
    this.showLabels = false;
    this.showChecklist = false;
    this.showDate = false;
    this.showCover = false;
  }

  toggleCoverCard(): void{
    this.showCover = !this.showCover;
    this.showMembers = false;
    this.showLabels = false;
    this.showChecklist = false;
    this.showDate = false;
    this.showAttachment = false;
  }

  onCloseCard(): void{
    this.cardService.showCard=false;
    this.showMembers = false;
    this.showLabels = false;
    this.showChecklist = false;
    this.showDate = false;
    this.showAttachment = false;
    this.showCover = false;

    this.dialogRef.close();
  }

  onCloseMembers(remove: boolean): void{ this.showMembers = remove;}

  onCloseLabels(remove: boolean): void{ this.showLabels = remove;}

  onCloseChecklist(remove: boolean): void{ this.showChecklist = remove;}

  onCloseDate(remove: boolean): void{ this.showDate = remove;}

  onCloseAttachment(remove: boolean): void{ this.showAttachment = remove;}

  onCloseCover(remove: boolean): void{ this.showCover = remove;}

  onAddNewMember(newMember: string): void{
    const alreadyAdded = this.card.members.filter(member => member === newMember) ;
    if (alreadyAdded.length > 0){
      const itemIndex: number = this.card.members.findIndex((member) => member === newMember);
      this.card.members.splice(itemIndex, 1);
    }
    else{
      this.card.members.push(newMember);
    }

    const updateMembersSub = this.cardService.changeCardMembers(this.card._id, this.card.members).subscribe();
    this.activeSubscriptions.push(updateMembersSub);
  }

  setLabel(color: string): void{
    const labelAdded = this.card.labels.filter((label) => label === color) ;
    if (labelAdded.length > 0){
      const itemIndex: number = this.card.labels.findIndex((label) => label === color);
      this.card.labels.splice(itemIndex, 1);
    }
    else{
      this.card.labels.push(color);
    }
  }

  onLabelChosen(label: string): void{
    if (label === 'green'){
      this.setLabel('green');
    }
    else if (label === 'yellow'){
      this.setLabel('yellow');
    }
    else if (label === 'orange'){
      this.setLabel('orange');
    }
    else if (label === 'red'){
      this.setLabel('red');
    }
    else if (label === 'purple'){
      this.setLabel('purple');
    }
    else if (label === 'blue'){
      this.setLabel('blue');
    }

    const updateLabelsSub = this.cardService.changeCardLabels(this.card._id, this.card.labels).subscribe();
    this.activeSubscriptions.push(updateLabelsSub);
  }

  onRecieveChecklistTitle(title: string): void{
      this.showChecklist = false;

      const createChecklistSub = this.checklistService
                                     .addChecklist(title)
                                     .pipe(
                                        switchMap((checklist) =>
                                        this.cardService.changeCardChecklists(this.card._id, checklist)),
                                        switchMap(() =>
                                        this.cardService.getCardById(this.card._id))
                                     )
                                     .subscribe((card) => (this.card = card));
      this.activeSubscriptions.push(createChecklistSub);
  }

  onSaveDate(date: Date): void{
    this.card.dueDate = date;

    const today: Date = new Date();
    const chosen: Date = new Date(date);

    if (chosen < today){
     this.card.completionStatus = 'overdue';
    }else{
      this.card.completionStatus = 'none';
    }

    this.showDate = false;

    const updateDateSub = this.cardService
                              .changeCardDueDate(this.card._id, this.card.dueDate, this.card.completionStatus)
                              .subscribe(() => {
    this.cardService.changeCardSubject.next({"cardId": this.cardId, "dueDate": chosen, "completionStatus": this.card.completionStatus});
                              });
    this.activeSubscriptions.push(updateDateSub);
  }

  onRemoveDate(remove: boolean): void{
   this.card.dueDate = null;
   this.card.completionStatus = 'none';

   this.showDate = false;

   const removeDateSub = this.cardService
                              .removeCardDueDate(this.card._id, remove, this.card.completionStatus)
                              .subscribe(() =>
                                this.cardService.changeCardSubject.next({"cardId": this.cardId, "dueDate": null}));
   this.activeSubscriptions.push(removeDateSub);

  }

  checkStatus(): void{
    const isChecked = (document.getElementById('dateCheckbox') as HTMLInputElement).checked;

    if (isChecked){
      this.card.completionStatus = 'complete';
    }
    else{
      const today: Date = new Date();
      const chosen: Date = new Date(this.card.dueDate);

      if (chosen < today){
        this.card.completionStatus = 'overdue';

      }else{
        this.card.completionStatus = 'none';
      }
    }
    const updateStatusSub = this.cardService
                              .changeCardDueDate(this.card._id, this.card.dueDate, this.card.completionStatus)
                              .subscribe( () =>
        this.cardService.changeCardSubject.next({"cardId": this.cardId, "completionStatus": this.card.completionStatus})
                              );
    this.activeSubscriptions.push(updateStatusSub);
  }

  onDisplayAttachment(files: FileList): void{
    Array.from(files).forEach(file => {
      this.uploadFile(file);
    });

    this.showAttachment = false;
  }

  uploadFile(file: File){
    const uploadFileSub = this.cardService
                              .putCardAttachment(this.card._id, file)
                              .pipe(
                                switchMap(() =>
                                 this.cardService.getCardById(this.card._id))
                              )
                              .subscribe((card) => {(this.card = card);});
    this.activeSubscriptions.push(uploadFileSub);
  }

  public fileSrc(index: number){ return environment.fileDownloadUrl + this.card.fileUrls[index];}

  public extName(index: number): string{
    const name = this.card.fileUrls[index];
    const p = name.lastIndexOf('.');
    if(p !== -1){
      return name.substr(p+1).toUpperCase();
    }else{
      return 'attachment';
    }
  }

  public onHideAttachments(): void{
    const changeVisabilitySub = this.cardService
                                    .changeHideAttachments(this.card._id)
                                    .pipe(
                                      switchMap(() =>
                                       this.cardService.getCardById(this.card._id))
                                    )
                                    .subscribe((card) => (this.card = card));
    this.activeSubscriptions.push(changeVisabilitySub);
  }

  public onDeleteAttachment(index: number): void{
    const deleteFileSub = this.cardService
                              .deleteCardAttachment(this.card._id, index)
                              .pipe(
                                switchMap(() =>
                                this.cardService.getCardById(this.card._id))
                              )
                              .subscribe((card) => (this.card = card))
    this.activeSubscriptions.push(deleteFileSub);
  }

  onCoverChosen(cover: string): void{
    this.card.cover = cover;
    const updateCoverSub = this.cardService.changeCardCover(this.card._id, this.card.cover).subscribe();
    this.activeSubscriptions.push(updateCoverSub);

    const updateTitleSub = this.cardService.changeCardTitle(this.card._id, this.card.title).subscribe(() => {
      this.cardService.changeCardSubject.next({"cardId": this.cardId, "cover": cover});
    });
    this.activeSubscriptions.push(updateTitleSub);
  }

  chosenCoverPath(): string{
    if (this.card.cover === 'green'){
      return '../../assets/green.jpg';
    }else if (this.card.cover === 'yellow'){
      return '../../assets/yellow.jpg';
    }else if (this.card.cover === 'orange'){
      return '../../assets/orange.jpg';
    }else if (this.card.cover === 'red'){
      return '../../assets/red.jpg';
    }else if (this.card.cover === 'purple'){
      return '../../assets/purple.jpg';
    }else if (this.card.cover === 'blue'){
      return '../../assets/blue.jpg';
    }
  }
}
