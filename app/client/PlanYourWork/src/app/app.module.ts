import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { BoardComponent } from './board/board.component';
import { ListComponent } from './list/list.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { BoardNavBarComponent } from './board-nav-bar/board-nav-bar.component';

import { CardComponent } from './card/card.component';
import { CardMembersComponent } from './card-members/card-members.component';
import { CardLabelsComponent } from './card-labels/card-labels.component';
import { CardCoverComponent } from './card-cover/card-cover.component';
import { CardChecklistComponent } from './card-checklist/card-checklist.component';
import { CardDateComponent } from './card-date/card-date.component';
import { CardAttachmentComponent } from './card-attachment/card-attachment.component';
import { ChecklistComponent } from './checklist/checklist.component';
import { ChecklistItemComponent } from './checklist-item/checklist-item.component';

import { FilterPipe } from './pipes/filter.pipe';
import {ToastrModule} from 'ngx-toastr';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { HttpClientModule } from '@angular/common/http'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProfileComponent } from './profile/profile.component';
import { NavlistComponent } from './navlist/navlist.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { EventEmitterService } from './services/event-emitter.service';
import { AuthGuard } from './guards/auth.guard';
import { BoardGuard } from './guards/board.guard';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './services/token-interceptor.service';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';

import { NgbDateAdapter, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { CustomAdapter } from './services/custom-adapter.service';
import { CustomDateParserFormatter } from './services/custom-parser.service';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    BoardComponent,
    ListComponent,
    SideBarComponent,
    BoardNavBarComponent,
    ProfileComponent,
    NavlistComponent,
    LoginComponent,
    RegisterComponent,
    CardComponent,
    CardMembersComponent,
    CardLabelsComponent,
    CardCoverComponent,
    CardChecklistComponent,
    CardDateComponent,
    CardAttachmentComponent,
    ChecklistComponent,
    ChecklistItemComponent,
    FilterPipe
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    DragDropModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatDialogModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [EventEmitterService, AuthGuard, BoardGuard, {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  },
  { provide: MatDialogRef, useValue: {} },
  { provide: MAT_DIALOG_DATA, useValue: {} },
  { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false} },
  {provide: NgbDateAdapter, useClass: CustomAdapter},
  {provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter}
  ],
  entryComponents: [
    CardComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
