import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Card } from '../models/card.model';
import { ListModel } from '../models/list.model';

@Injectable({
  providedIn: 'root'
})
export class ListService {

  private lists: Observable<ListModel[]>;
  private readonly listsUrl = "http://localhost:5000/api/list/";

  constructor(private http: HttpClient) {
    this.refreshLists();
  }

  private refreshLists(): Observable<ListModel[]> {
    this.lists = this.http.get<ListModel[]>(this.listsUrl);
    return this.lists;
  }

  public getLists(): Observable<ListModel[]> {
    return this.lists;
  }

  public getListById(listId: string): Observable<ListModel> {
    return this.http.get<ListModel>(this.listsUrl + listId);
  }

  public addNewList(listName: string): Observable<ListModel> {
    const body = {
      name: listName
    };

    return this.http.post<ListModel>(this.listsUrl, body);
  }

  public deleteList(boardId: string, listId: string): Observable<Object> {
    return this.http.delete(this.listsUrl + boardId + "/" + listId);
  }

  public changeListName(listId: string, listName: string): Observable<Object> {
    const body = {
      name: listName
    };

    return this.http.put(this.listsUrl + listId, body);
  }

  public updateTasksInList(listId: string, tasks: string[]): Observable<Object> {
    const body = {
      tasks: tasks
    };

    return this.http.put(this.listsUrl + listId, body);
  }
}
