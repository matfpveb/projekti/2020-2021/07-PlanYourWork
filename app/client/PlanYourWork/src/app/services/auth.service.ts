import { Injectable } from '@angular/core';
import {User} from '../models/user.model';
import { Subject, Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { MessageService } from './message.service';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public readonly currentUser: Subject<User> = new Subject<User>();
  public readonly currentUserObservable: Observable<User> = this.currentUser.asObservable();

  private readonly projectsUrl="http://localhost:5000/projects/";
  private readonly usersUrl="http://localhost:5000/api/users/" ;
  private readonly TOKENS = {
    ACCESS_TOKEN: "JWT_TOKEN",
    REFRESH_TOKEN: "REFRESH_TOKEN"
  };


  constructor(private http: HttpClient, private messageService:MessageService) {}

  sendMessage(which : String, update: boolean, text = "") : void{ this.messageService.sendMessage(which, update, text);}

  clearMessages( msg : String) : void{ this.messageService.clearMessages();}

  public getToken(typeAcess:boolean): string | null {
    const tokenType = (typeAcess) ? this.TOKENS.ACCESS_TOKEN : this.TOKENS.REFRESH_TOKEN;
    const token: string | null = localStorage.getItem(tokenType);
    if (!token) {
      return null;
    }
    return token;
  }

  public getDataFromToken(): User {
    const token = this.getToken(true);
    if (!token) {
      return null;
    }
    const payloadString = token.split(".")[1];
    const userDataJSON = window.atob(payloadString);
    const payload: User = JSON.parse(userDataJSON);
    return payload;
  }

  public setToken(jwt: string,refresh:string=null): void {
    localStorage.setItem(this.TOKENS.ACCESS_TOKEN, jwt);
    if(refresh){
      localStorage.setItem(this.TOKENS.REFRESH_TOKEN, refresh);
    }
  }

  public removeTokens(): void {
    localStorage.removeItem(this.TOKENS.ACCESS_TOKEN);
    localStorage.removeItem(this.TOKENS.REFRESH_TOKEN);
  }

  public sendUserDataIfExists(): User {
    const payloadData: User = this.getDataFromToken();
    const user: User = payloadData
      ? new User(payloadData._id, payloadData.username, payloadData.email, payloadData.fullname, payloadData.projects, payloadData.imgUrl)
      : null;
    this.currentUser.next(user);
    return user;
  }

  public getUserFromToken(accessToken: string, refreshToken:string=null): User {

    this.setToken(accessToken,refreshToken);
    return this.sendUserDataIfExists();
  }

  public getLoggedInUsername(): String { return this.sendUserDataIfExists().username;}

  public  createAUser(data):Observable<User>{
    const res = this.http.post<User>(this.usersUrl+"register",data);
    this.sendMessage('users', true, "user created");
    return res;
  }
  public  loginUser(data):Observable<Object>{
      const login = this.http.post<User>(this.usersUrl+"login",data);
      return login;

  }
  public loggedIn(){ return !! localStorage.getItem('JWT_TOKEN');}

  logout():Observable<Object> {
    const reftoken = localStorage.getItem(this.TOKENS.REFRESH_TOKEN)
    const logout =  this.http.post<any>(this.usersUrl+"logout", {'reftoken': reftoken});


    return  logout
  }

  refreshToken():Observable<Object>  {
    return this.http.post<any>(this.usersUrl+"refresh_token", {
      'reftoken': this.getToken(false)
    })
  }

  public  deleteUser():Observable<Object>{
    const removeUser = this.http.delete<User>(this.usersUrl+this.sendUserDataIfExists().username);
    return removeUser;

  }
  public errorHandler(error:HttpErrorResponse){
    this.sendMessage('project', false, "error");
    return throwError(error.error.message || "Server error");
  }

}
