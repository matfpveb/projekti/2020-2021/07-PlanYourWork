import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';

@Component({
  selector: 'app-card-checklist',
  templateUrl: './card-checklist.component.html',
  styleUrls: ['./card-checklist.component.css']
})
export class CardChecklistComponent implements OnInit, AfterViewChecked {

  @Output()
  emitShowChecklist: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Output()
  emitChecklistTitle: EventEmitter<string> = new EventEmitter<string>();

  @ViewChild('checklistTitle')
  private checklistTitle: ElementRef;

  title: string;

  constructor() { this.title = 'Checklist';}

  ngOnInit(): void {}

  ngAfterViewChecked(): void { this.checklistTitle.nativeElement.focus();}

  onCloseChecklist(): void{ this.emitShowChecklist.emit(false);}

  onSendTitle(): void{
    if(this.title !== ''){
      this.emitChecklistTitle.emit(this.title);
    }
  }

}
