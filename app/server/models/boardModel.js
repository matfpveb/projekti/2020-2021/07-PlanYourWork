const mongoose = require('mongoose');

const boardSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        required: true
    },
    lists: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'list',
        required: true
    }]
});

const boardModel = mongoose.model('board', boardSchema);

module.exports = boardModel;
