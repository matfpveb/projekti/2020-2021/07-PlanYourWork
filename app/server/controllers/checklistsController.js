const Checklist = require('../models/checklistsModel');
const mongoose = require('mongoose');

module.exports.getChecklists = async (req, res, next) => {
    try{
        const checklists = await Checklist.find({}).exec();
        res.status(200).json(checklists);
    }catch(err){
        next(err);
    }
};

module.exports.getChecklistById = async (req, res, next) => {
    
    try{    
        const checklist = await Checklist.findById(req.params.id).exec();
        if(checklist){
            res.status(200).json(checklist);
        }else{
            res.status(404).send();
        }
    }catch(err){
        next(err);
    }

};

module.exports.createChecklist = async (req, res, next) => {
    if(!req.body.title){
        res.status(400).send();
    }else{
        try{
            const newChecklist= new Checklist({
                _id: new mongoose.Types.ObjectId(),
                title: req.body.title
            });   

            await newChecklist.save();

            res.status(201).json(newChecklist);
        }catch(err){
            next(err);
        }
    }
};

module.exports.updateChecklistById = async (req, res, next) => {
   
    try{
        const checklist = await Checklist.findById(req.params.id).exec();

        if(checklist){
            const updateChecklist = req.body;

            if(updateChecklist.title){
                checklist.title = updateChecklist.title;

            }else if(updateChecklist.progressPercentage >= 0){
                checklist.progressPercentage = updateChecklist.progressPercentage;

            }else if(updateChecklist.items){
                checklist.items = updateChecklist.items;

            }else if(updateChecklist.updateItemsVisability){
                checklist.hideItems = updateChecklist.hideItems;
                checklist.hideItemsText = updateChecklist.hideItemsText;
            }else{
                return res.status(400).send({ message: "This request is not valid!" });
            }

            await checklist.save();
            res.status(200).send();

        }else{
            res.status(404).send();
        } 
    }catch(err){
        next(err);
    }   
};

module.exports.deleteChecklistById = async (req, res, next) => {
    try{
        const checklist = await Checklist.findById(req.body.checklistId).exec();

        if(checklist){
            await Checklist.findByIdAndDelete(req.body.checklistId).exec();
            res.status(200).send();
        }else{
            res.status(404).send();
        }
    }catch(err){
        next(err);
    }
};

module.exports.addItem = async (req, res, next) => {
    try{
        const checklist = await Checklist.findById(req.params.id).exec();

        if(checklist){
            const ind = checklist.items.findIndex((item) => item.name === req.body.name);
            if(ind === -1){
                checklist.items.push({name: req.body.name, checked:false});
                await checklist.save();
                res.status(200).send({name:req.body.name});
            }
            else{
                res.status(400).send({message : "Checklist item with this name already exists"});
            }
            
        }else{
            res.status(404).send();
        }
    }catch(err){
        next(err);
    }
};

module.exports.deleteItem = async (req, res, next) => {
    try{
        const checklist = await Checklist.findById(req.params.id).exec();

        if(checklist){
            checklist.items = checklist.items.filter((item) => item.name!==req.body.itemName);
            await checklist.save();
            res.status(200).send();
        
            
        }else{
            res.status(404).send();
        }
    }catch(err){
        next(err);
    }
};

module.exports.checkItem = async (req, res, next) => {
    try{
        const checklist = await Checklist.findById(req.params.id).exec();

        if(checklist){
            const ind = checklist.items.findIndex((item) => item.name === req.body.name);
            if( ind === -1){
                res.status(404).send({message : "item not found"});
            }
            else{
                checklist.items[ind].checked = req.body.checked;
                await checklist.save();
                res.status(200).send();
            }
            
        }else{
            res.status(404).send();
        }
    }catch(err){
        next(err);
    }
};
module.exports.hideItems = async (req, res, next) => {
    try{
        const checklist = await Checklist.findById(req.params.id).exec();

        if(checklist){
            checklist.hideItems = req.body.hideCheckedItems;
            checklist.hideItemsText = req.body.hideCheckedItemsText;
            await checklist.save();
            res.status(200).send();
        }else{
            res.status(404).send();
        }
    }catch(err){
        next(err);
    }
};