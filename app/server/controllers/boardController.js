const Board = require('../models/boardModel');
const mongoose = require('mongoose');

module.exports.getBoards = async (req, res, next) => {
    try {
        const boards = await Board.find({}).exec();
        res.status(200).json(boards);
    } catch(err) {
        next(err);
    }
};

module.exports.getBoardById = async (req, res, next) => {
    try {
        const board = await Board.findById(req.params.boardId).populate('lists').exec();

        if (board) {
            res.status(200).json(board);
        } else {
            res.status(400).send();
        }
    } catch(err) {
        next(err);
    }
};

module.exports.createBoard = async (req, res, next) => {
    if (!req.body.name) {
        res.status(400).send();
    } else {
        try {
            const newBoard = new Board({
                _id: new mongoose.Types.ObjectId(),
                name: req.body.name,
                lists: []
            });

            req.body.newBoard = newBoard;
            await newBoard.save();
            next();
        } catch(err) {
            next(err);
        }
    }
};

module.exports.updateBoardById = async (req, res, next) => {
    try {
        const board = await Board.findById(req.params.boardId).exec();

        if (board) {
            const updateBoardData = req.body;

            if (updateBoardData.name)
                board.name = updateBoardData.name;

            if (updateBoardData.list)
                board.lists.push(updateBoardData.list);

            await board.save();

            res.status(200).send();

        } else {
            res.status(404).send();
        }

    } catch(err) {
        next(err);
    }
};

module.exports.deleteBoardById = async (req, res, next) => {
    try {
        const board = await Board.findById(req.params.board_id).exec();
        if (board) {
            await Board.findByIdAndDelete(req.params.board_id).exec();
            next();
        } else {
            res.status(404).send();
        }

    } catch(err) {
        next(err);
    }
};

module.exports.deleteListInBoard = async (req, res, next) => {
    try {
        const board = await Board.findById(req.params.boardId).exec();
        if (board) {
            const listIndex = board.lists.findIndex((id) => id === req.params.listId);
            board.lists.splice(listIndex, 1);
            await board.save();
            next();
        } else {
            res.status(404).send();
        }

    } catch(err) {
        next(err);
    }
};
